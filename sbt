#!/bin/python

TAG = 'master'
GIT = 'https://gitlab.com/peterlavalle/sbt.bin.git'


def main():
	import os, shutil

	if not os.path.isdir('.git/'):
		git_init()

	os.system('git submodule init')
	os.system('git submodule update')

	if not os.path.isdir('.sbt.bin/'):
		sbt_bin()

	os.system('git submodule init')
	os.system('git submodule update')

	if not os.path.isdir('project/'):
		os.mkdir('project/')

	if not os.path.isfile('.gitignore'):
		shutil.copyfile('.sbt.bin/client/.gitignore', '.gitignore')

	if not os.path.isfile('project/build.properties'):
		shutil.copyfile('.sbt.bin/client/build.properties', 'project/build.properties')

	if not os.path.isfile('build.sbt'):
		shutil.copyfile('.sbt.bin/client/build.sbt', 'build.sbt')

	import sys
	sbt = ['java', '-jar', '.sbt.bin/sbt-launch.jar']
	first = True
	for a in sys.argv:
		if first:
			first = False
		else:
			sbt.append(a)

	import subprocess
	subprocess.run(sbt)

def git_init():
	
	print ('Want me to create a new project and setup .git? (y/)')

	doit = str(input()[:1]).lower()
	doit = ('y' == doit) or ('' == doit)

	if not doit:
		raise Exception("don't know how to work with a non-vc project")

	os.system('git init')
	if not os.path.isdir('.git/'):
		raise Exception('git init failed')

def sbt_bin():

	os.system('git submodule init')
	os.system('git submodule add -b ' + TAG + ' ' + GIT + ' .sbt.bin/')

main()
