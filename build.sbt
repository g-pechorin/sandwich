
/// ====
// monorepo config block

import java.io.File

val hgRoot: File = {
	var root = file("").getAbsoluteFile

	while (!(root / ".sbt.bin/scala.conf").exists())
		root = root.getAbsoluteFile.getParentFile.getAbsoluteFile

	root
}

def conf: String => String = {
	import com.typesafe.config.ConfigFactory

	(key: String) =>
		ConfigFactory.parseFile(
			hgRoot / ".sbt.bin/scala.conf"
		).getString(key)
}
// end of monorepo config block

lazy val all: Seq[Def.Setting[_]] =
	Seq(
		scalaVersion := conf("scala.version"),
		scalacOptions ++= conf("scala.options").split("/").toSeq,
		resolvers += Classpaths.typesafeReleases,
		resolvers += Resolver.mavenCentral,
		resolvers += Resolver.jcenterRepo,
		resolvers += "jitpack" at "https://jitpack.io",

		// magic to get templates into the build
		Compile / resourceDirectory := (Compile / scalaSource).value,
		Test / resourceDirectory := (Test / scalaSource).value,
	)

// end of standard stuff
/// ---

lazy val root = {
	(project in file("."))
		.settings(all: _ *)
}

name := "???"
organization := "???"
